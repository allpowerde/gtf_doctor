#!/usr/bin/env python


################################################################################
#   June 7, 2012
#   Authors: Vlad Makarov
#   Language: Python
#   OS: UNIX/Linux, MAC OSX
#   Copyright (c) 2012, The Mount Sinai School of Medicine

#   Available under BSD  licence

#   Redistribution and use in source and binary forms, with or without modification,
#   are permitted provided that the following conditions are met:
#
#   Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
#
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#   IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
#   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#   BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
#   OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################

import sys
import os.path
import os
import gzip
import operator


""" is file exists """
def isExist(filename):
    if os.path.exists(filename) and os.path.isfile(filename):
        return True
    else:
        return False

""" Reads 2 column file to dictionary
    where key is transcript_id """

def read2map(filename):
    tss_ids = {}
    if isExist(filename):
        fh = open(filename, "r")
        for line in fh:
            fields=line.split('\t')
            k=fields[0].strip()
            v=fields[1].strip()
            tss_ids[k] = v

    return tss_ids


""" Searches dictionary by key value,
    that is transcript_id """
def do_search(search_str, tss_ids):
    if tss_ids.has_key(search_str):
        return tss_ids[search_str]
    else:
        return ''


""" Converts string to boolean """
def str2bool(v):
  return v.lower() in ["y", "yes", "true", "t", "1"]


""" Determines if the file is zipped
    and returns appropriate file handler """
def getFh(filename):
  fh = open(filename, "r")
  if filename.endswith('gz'):
    fh = gzip.open(filename, 'rb')
  return fh



""" Parse key-values pairs"""
def parse_field(text, key, sep1, sep2):
    fields = text.strip().split(sep1)
    onefield=set([])
    for f in fields:
        pairs = f.strip().split(sep2)
        if str(pairs[0]).strip() == str(key).strip():
            return str(pairs[1])
    return ''

""" read through GTF file and add tss_id and p_id to the transcripts"""
def parse_info_from_cuffcomp(filename, tss_id_file, outfile, comment_char='#'):
    print "Reading through GTF file and add tss_id and p_id to the transcripts"
    tss_ids=read2map(tss_id_file)
    if len(tss_ids)==0:
        print (tss_id_file + " is empty or has no records")

    else:
        print str(len(tss_ids)) + ' transcripts records are loaded'
        ## avoid ovewriting file
        if outfile==filename:
            outfile=filename.replace('.gtf', '.doctored.gtf')
        print 'Output will be written to ' + outfile
        fh_out = open(outfile, "w")


        fh=getFh(filename)

        count=0
        for line in fh:
            line = line.strip()
            count=count+1
            if line.startswith(comment_char):
                #print line
                fh_out.write(line+'\n')
            else:
                fields = line.split('\t')
                info=fields[8].strip()
                transcript_id=str(parse_field(text=info, key='transcript_id', sep1=';', sep2=' '))
                tss_id=do_search(search_str=transcript_id, tss_ids=tss_ids)
                l=''
                if tss_id=='':
                    l=line
                else:
                    l=line+' ' + tss_id+';'

                fh_out.write(l+'\n')

            if count % 100000 ==0:
                print ('Pass ' + str(count) + ' lines')
        fh_out.close()
        fh.close()





## Run or print usage and bail
if (len(sys.argv) > 3):
    filename=sys.argv[1]
    tss_id_file=sys.argv[2]
    outfile=sys.argv[3]
    parse_info_from_cuffcomp(filename=filename, tss_id_file=tss_id_file, outfile=outfile, comment_char='#')
else:
    print ('gtf_doctor: Utility to add tss_id and p_id tags to GTF file, calculate GC content and convert GTF, GFF, BED and VCF files between b37 and hg19 formats')
    print ('usage:    python insert_tss_id.py [gtf_file [tss_id_file] [out_gtf_file]')
