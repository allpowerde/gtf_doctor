#!/bin/sh -e

# PBS
#PBS -N DoctorGTF
#PBS -q serial
#PBS -l walltime=24:00:00
#PBS -o o
#PBS -e o

# SGE
#$ -N DoctorGTF
#$ -q serial
#$ -l walltime=24:00:00
#$ -o o
#$ -e o

function usage {
echo -e "usage: $(basename $0) -r REFERENCE -i INPUT.GTF [OPTIONS]

Script to modify Gencode GTF files for use with the tuxedo suite and RNAseq-QC

required:
  -r | --reference <file>   reference genome
  -i | --input <file>       gtf input
  
options:
  -b | --bedtools <str>     bedtools module
  -c | --cufflinks <str>    cufflinks module
  -d | --doctor <dir>       location of gtf-doctor scripts
  -k | --keep_seleno        keep seleno tagged entries (can cause problems with cufflinks)
  -f | --force              overwrite existing files
  -s | --suffix <str>       suffix to use for doctored file (default .doctored.gtf)
"
exit
}


INPUTGTF=
REFERENCE=
OUTPUTDIR=$(pwd)

BEDTOOLS_MODULE=
CUFFLINKS_MODULE=
DOCTORDIR=$(dirname $0)
SUFFIX=".doctored.gtf"
FORCE=FALSE
MAKEB37=FALSE
KEEPSELENO=FALSE

if [ ! $# -gt 3 ]; then usage ; fi

while [ "$1" != "" ]; do
    case $1 in
        -r | --reference )      shift; REFERENCE=$1 ;; # reference genome
        -i | --input )          shift; INPUTGTF=$1 ;;  # input gtf file

        -b | --bedtools )       shift; BEDTOOLS_MODULE=$1 ;; # the bedtools module to load
        -c | --cufflinks )      shift; CUFFLINKS_MODULE=$1 ;; # the cufflinks module to load
        -d | --doctor )         shift; DOCTORDIR=$1 ;; # dir of gtf-doctor executables
        -k | --keep_seleno )    KEEPSELENO=TRUE;;      # keep seleno tagged entries
        -s | --suffix )         shift; SUFFIX=$1 ;; # suffix
        -f | --force )          FORCE=TRUE;; # overwrite existing
        -h | --help )           usage ;;
        * )                     echo "don't understand "$1
    esac
    shift
done

## print command and all arguments
echo "[NOTE] Command: $0 $@"

## load modules

[ -n "$CUFFLINKS_MODULE" ] && module load $CUFFLINKS_MODULE
[ -n "$BEDTOOLS_MODULE" ]  && module load $BEDTOOLS_MODULE

## check requirements

if [ -z "$REFERENCE" ] || [ ! -f $REFERENCE ]; then
    echo "[ERROR] Reference genome does not exist: $REFERENCE"
    exit 1
else
    echo "[NOTE] Reference: $REFERENCE"
fi

if  [ -z "$INPUTGTF" ] || [ ! -f $INPUTGTF ]; then
    echo "[ERROR] Input GTF file not found: $INPUTGTF"
    exit 1
else
    echo "[NOTE] GTF input: $INPUTGTF"
fi

if ! hash cuffcompare; then
    echo "[ERROR] cuffcompare not in PATH"
    exit 1
else
    echo "[NOTE] cuffcompare version: "$(cuffcompare 2>&1 | head -n 1)
fi

if ! hash perl; then
    echo "[ERROR] perl not in PATH"
    exit 1
else
    echo "[NOTE] perl: "$(which perl)
    echo "[NOTE] perl Version: "$(perl -v | head -n 2 | tail -n 1)
fi

if ! hash python; then
    echo "[ERROR] python not in PATH"
    exit 1
else
    echo "[NOTE] python: "$(which python)
    echo "[NOTE] python version: "$(python --version 2>&1 | head -n 1)
fi

if ! hash fastaFromBed; then
    echo "[ERROR] fastaFromBed not in PATH (BEDtools)"
    exit 1
else
    echo "[NOTE] fastaFromBed: "$(which fastaFromBed)
    echo "[NOTE] fastaFromBed version: "$(fastaFromBed 2>&1 | head -n 3 | tail -n 1)
fi

if [ ! -f ${DOCTORDIR}/insert_tss_id.py ] || [ ! -f ${DOCTORDIR}/gc_content_exons.py ] ; then
    echo "[ERROR] insert_tss_id.py not in PATH"
    exit 1
else
    echo "[NOTE] doctor: ${DOCTORDIR}"
fi

echo "[NOTE] The doctor is in! Start procedure..."

OUTPUTDIR=$(dirname $INPUTGTF)

## name of original "X.gtf" file will be ovewritten by the "X.doctored.gtf"
OUTPUT=${INPUTGTF##*/}
## output without suffix
OUTPUT=${OUTPUT%.*}$SUFFIX

if [ "${OUTPUTDIR}/$OUTPUT" == "$INPUTGTF" ];then
    echo "[ERROR] Output file cannot have the same name as the input gtf"
fi

echo "[NOTE] Write doctored gtf to: "${OUTPUTDIR}/$OUTPUT
if [ -f ${OUTPUTDIR}/$OUTPUT ]; then
    echo "[WARN] output file exists already"
    if [ "$FORCE" != "TRUE" ]; then
        exit 1
    fi
fi

echo "[NOTE] Running cuffcompare to add the appropriate attributes to your custom GTF file ${INPUTGTF}"
COMMAND="cuffcompare  -o ${OUTPUTDIR}/${OUTPUT}_cuffcmp -r ${INPUTGTF} -s ${REFERENCE} ${INPUTGTF} ${INPUTGTF}"
echo "[NOTE] Command: $COMMAND" && eval $COMMAND

echo "[NOTE] Parse INFO field from the GTF file (tss_id and p_id fields)"
awk 'BEGIN { FS = "\t" };{ print $9 }' ${OUTPUTDIR}/${OUTPUT}_cuffcmp.combined.gtf > ${OUTPUTDIR}/${OUTPUT}_cuffcmp.combined.gtf.tmp

echo "[NOTE] Parse tss_id"
awk 'BEGIN { FS = ";" };{ print $5, $8, $9 }' ${OUTPUTDIR}/${OUTPUT}_cuffcmp.combined.gtf.tmp | sort | uniq > ${OUTPUTDIR}/${OUTPUT}_tss_p_id.txt

perl -pi -e 's/ oId //g' ${OUTPUTDIR}/${OUTPUT}_tss_p_id.txt
perl -pi -e 's/  tss_id/\ttss_id/g' ${OUTPUTDIR}/${OUTPUT}_tss_p_id.txt
perl -pi -e 's/  p_id/; p_id/g' ${OUTPUTDIR}/${OUTPUT}_tss_p_id.txt

echo "[NOTE] read through GTF file and add tss_id and p_id to the transcripts"
python ${DOCTORDIR}/insert_tss_id.py ${INPUTGTF} ${OUTPUTDIR}/${OUTPUT}_tss_p_id.txt $OUTPUTDIR/${OUTPUT}

if [ "$KEEPSELENO" != "TRUE" ]; then
    echo "[NOTE] remove entries tagged with seleno"
    grep -v -i "tag \"seleno\"" $OUTPUTDIR/${OUTPUT} > $OUTPUTDIR/${OUTPUT}.tmp
    mv $OUTPUTDIR/${OUTPUT}.tmp $OUTPUTDIR/${OUTPUT}
fi

echo "[NOTE] Create .gc file with GC content for each transcript."
python ${DOCTORDIR}/gc_content_exons.py $OUTPUTDIR/${OUTPUT} ${REFERENCE}
python ${DOCTORDIR}/gc_content_exons.py $OUTPUTDIR/${INPUTGTF##*/} ${REFERENCE}

echo "[NOTE] cleanup"
rm ${OUTPUTDIR}/${OUTPUT}_cuffcmp* ${OUTPUTDIR}/${OUTPUT}_tss_p_id.txt

echo "[SUCCESS]: Medical procedure finished successfully."
